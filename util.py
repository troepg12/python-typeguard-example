from typeguard import typechecked
from typing import Optional

attr_cnt = 0


@typechecked
def build_typesafe_property(
        type_: type,
        name: Optional[str] = None) -> property:
    if name is None:
        global attr_cnt
        name = attr_cnt
        attr_cnt += 1
    # don't use private prefix '__' to avoid name mangling
    actual_var_name = 'magic_string_private_____{}'.format(name)

    @typechecked
    def getter(self) -> type_:
        if not hasattr(self, actual_var_name):
            raise AttributeError('variable is not initialized')
        return getattr(self, actual_var_name)

    @typechecked
    def setter(self, value: type_):
        setattr(self, actual_var_name, value)

    return property(getter, setter)
