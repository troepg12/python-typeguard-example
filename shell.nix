{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/4789953e5c1ef6d10e3ff437e5b7ab8eed526942.tar.gz") {} }:
pkgs.mkShell {
  buildInputs = [
    (pkgs.python37.withPackages (ps: [ps.typeguard]))
  ];
}
