from typeguard import typechecked
import util


@typechecked
class ExampleClass:
    myvar = util.build_typesafe_property(str)
    myothervar = util.build_typesafe_property(str)

    def __init__(self):
        self.myvar = 'initial value'

    def mymethod(self, cnt: int) -> str:
        return cnt * self.myvar

    def wrongreturn(self) -> int:
        # looks like an int, but is a string
        return '17'

    def pow(self, base: float, exponent: float) -> float:
        return base**exponent
