from definition import ExampleClass
import unittest


class TestTypechecks(unittest.TestCase):
    def setUp(self):
        self.ex = ExampleClass()

    def test_basic_scenario(self):
        self.assertEqual(self.ex.mymethod(4), 'initial valueinitial value'
                         'initial valueinitial value')
        self.ex.myvar = 'msg'
        self.assertEqual(self.ex.mymethod(3), 'msgmsgmsg')

    def test_assignment(self):
        with self.assertRaises(TypeError):
            self.ex.myvar = 17

    def test_param_type(self):
        with self.assertRaises(TypeError):
            self.ex.mymethod('4')

    def test_param_None(self):
        with self.assertRaises(TypeError):
            self.ex.mymethod(None)

    def test_return_type(self):
        with self.assertRaises(TypeError):
            self.ex.wrongreturn()

    def test_typecast_int2float(self):
        self.ex.pow(int(2), 0.5)

    def test_typecast_float2int(self):
        with self.assertRaises(TypeError):
            self.ex.mymethod(2.0)

    def test_unitialized(self):
        # no error: initialized in __init__()
        self.ex.myvar

        with self.assertRaises(AttributeError):
            # error: not initialized
            self.ex.myothervar

        self.ex.myvar = 'abc'
        self.ex.myothervar = 'def'

        # does not throw anymore
        self.ex.myvar
        self.ex.myothervar

    def test_property_collision(self):
        self.ex.myvar = 'abc'
        self.ex.myothervar = 'ddd'
        ex = ExampleClass()
        ex.myvar = 'ggg'
        ex.myothervar = '123'
        self.assertEqual('abc', self.ex.myvar)
        self.assertEqual('ddd', self.ex.myothervar)
        self.assertEqual('ggg', ex.myvar)
        self.assertEqual('123', ex.myothervar)

        self.ex.myvar = 'xxx'
        ex.myothervar = '126378'
        self.assertEqual('xxx', self.ex.myvar)
        self.assertEqual('ddd', self.ex.myothervar)
        self.assertEqual('ggg', ex.myvar)
        self.assertEqual('126378', ex.myothervar)


if '__main__' == __name__:
    unittest.main()
