# Python typeguard example
Python 3 added type annotations according to [PEP 484](https://www.python.org/dev/peps/pep-0484/),
though they are not enforced by default.

[typeguard](https://github.com/agronholm/typeguard) is a library that enforces these annotations.

This is an example how those type annotations could be used.

> Please install typeguard yourself, e.g. from [pip](https://pypi.org/project/typeguard/).
> This project provides a nix file to load it into your environment, invoke `nix-shell` inside this directory to use it.

Recommended reading order from top to bottom:

| File            | content                                                                                                   |
|-----------------|-----------------------------------------------------------------------------------------------------------|
| `definition.py` | class that uses the typeguard `@typechecked` annotation                                                   |
| `test.py`       | tests to check if the type annotations work                                                               |
| `util.py`       | helper to create class properties (to serve as typechecked class attributes)                              |
| `shell.nix`     | auxilliary definition of developer environment using [nix](https://nixos.org/guides/dev-environment.html) |

To run the tests execute `python3 test.py`.

